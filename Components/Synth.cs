using System;

namespace Components{
    public class Synth {
        
        public long[,] _matrix;
        public int _variables;
        public int _restrictions;

        public Synth( int variables, int restrictions ){
            _variables = variables;
            _restrictions = restrictions;
            _matrix = new long[variables, restrictions];
        }

        public void Start( ){
            
            for(int i = 0; i<_variables; i++){
                for(int j = 0; j<_restrictions; j++){
                    _matrix[i,j] = 1;
                }
            }

            for(int i = 0; i<_variables; i++){
                for(int j = 0; j<_restrictions; j++){
                    _matrix[i,j] = FindPrimeNumber(1000+ErrorTax());
                }
            }
        }

        public int ErrorTax( ){
            Random r = new Random();
            return r.Next(0, 300);
        }


public long FindPrimeNumber(int n){
    int count = 0;
    long a = 2;
    while(count<n)    {
        long b = 2;
        int prime = 1; // to check if found a prime
        while(b * b <= a) {
            if(a % b == 0) {
                prime = 0;
                break;
            }
            b++;
        }
        if(prime > 0) {
            count++;
        }
        a++;
    }
    return (--a);
}
    }
}
/*
t = 0;
for i=1 to n do
    for j=1 to m do
        t = t+1;
 */