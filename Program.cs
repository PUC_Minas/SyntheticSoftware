﻿using System;
using Components;

namespace Sintetico
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = Int32.Parse( Console.ReadLine() );
            var y = Int32.Parse( Console.ReadLine() );
            Synth teste = new Synth(x, y);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            teste.Start();
            watch.Stop();

            var elapsed = watch.ElapsedMilliseconds;
            Console.WriteLine("Hello World! "+elapsed+" ms");
        }
    }
}
