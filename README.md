# Software Sintetico


**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018<br />

### Objetivo
Implementar um software sintético que simula as instruções a serem usadas no software em sí e assim poder ser aplicado em um ambiente de produção para teste

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [C#](https://dotnet.github.io/)<br />
Banco de dados: Não utiliza<br />

### Execução

    $ dotnet restore
    $ dotnet run
    

### Contribuição

Esse projeto está concluído e livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->